### Soal Nomor 2
Untuk menganalisis kompleksitas waktu, mari kita lihat loop utama dalam fungsi `isBalanced(String input)`. Loop ini akan melakukan iterasi sebanyak karakter dalam string input, yang berukuran `n`. Di dalam loop, operasi-operasi yang dilakukan adalah operasi push dan pop pada stack, serta pembandingan karakter.

Kompleksitas ruang tergantung pada penggunaan stack. Pada kondisi terburuk, ketika semua karakter buka (seperti '(', '{', atau '[') disertakan tetapi tidak ada karakter penutup (')', '}', atau ']') yang cocok, stack akan berisi semua karakter buka sebelum mencapai karakter penutup yang pertama.

Sehingga secara keseluruhan, kodingan ini memiliki kompleksitas linier, yang artinya waktu eksekusi dan penggunaan memori bertambah secara proporsional dengan panjang input.


### Documentation
cek pada file GITS BRI.postman_collection.json
