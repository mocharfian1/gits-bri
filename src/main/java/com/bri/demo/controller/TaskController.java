package com.bri.demo.controller;

import com.bri.demo.model.BalancedBracketRequestBody;
import com.bri.demo.model.PalindromeRequestBody;
import com.bri.demo.model.RequestWeightStringsQuery;
import com.bri.demo.service.BalancedBracketService;
import com.bri.demo.service.PalindromeService;
import com.bri.demo.service.WeightedStringsService;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class TaskController {

    private final WeightedStringsService taskService;
    private final PalindromeService palindromeService;
    private final BalancedBracketService balancedBracketService;

    @PostMapping("/checkQueries")
    public List<String> checkQueries(@RequestBody RequestWeightStringsQuery body) {
        return taskService.checkQueries(body.getInput(), body.getQueries());
    }

    @PostMapping("/balancedBracket")
    public String isBalanced(@RequestBody BalancedBracketRequestBody body) {
        return balancedBracketService.isBalanced(body.getInput());
    }

    @PostMapping("/highestPalindrome")
    public String getHighestPalindrome(@RequestBody PalindromeRequestBody body) {
        return palindromeService.highestPalindrome(body.getInput(), body.getK());
    }
}
