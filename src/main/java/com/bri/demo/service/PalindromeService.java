package com.bri.demo.service;

import org.springframework.stereotype.Service;

@Service
public class PalindromeService {

    public String highestPalindrome(String s, int k) {
        char[] chars = s.toCharArray();
        if (!canFormPalindrome(chars, k, 0, chars.length - 1)) {
            return "-1";
        }
        maximizePalindrome(chars, k, 0, chars.length - 1);
        return new String(chars);
    }

    private boolean canFormPalindrome(char[] chars, int k, int left, int right) {
        if (left >= right) return true;
        if (chars[left] != chars[right]) {
            if (k == 0) return false;
            return canFormPalindrome(chars, k - 1, left + 1, right - 1);
        }
        return canFormPalindrome(chars, k, left + 1, right - 1);
    }

    private void maximizePalindrome(char[] chars, int k, int left, int right) {
        if (left > right) return;

        if (chars[left] != chars[right]) {
            char maxChar = (char) Math.max(chars[left], chars[right]);
            chars[left] = maxChar;
            chars[right] = maxChar;
            k--;
        }

        maximizePalindrome(chars, k, left + 1, right - 1);

        if (left < right && chars[left] != '9' && k > 0) {
            if (chars[left] == chars[right]) {
                if (k >= 2) {
                    chars[left] = '9';
                    chars[right] = '9';
                    k -= 2;
                }
            } else if (k >= 1) {
                chars[left] = '9';
                chars[right] = '9';
                k--;
            }
        }
    }
}