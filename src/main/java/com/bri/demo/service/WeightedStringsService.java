package com.bri.demo.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class WeightedStringsService {

    public List<String> checkQueries(String input, List<Integer> queries) {
        Set<Integer> weights = calculateWeights(input);
        List<String> results = new ArrayList<>();

        for (Integer query : queries) {
            results.add(weights.contains(query) ? "Yes" : "No");
        }

        return results;
    }

    private Set<Integer> calculateWeights(String input) {
        Set<Integer> weights = new HashSet<>();
        int length = input.length();

        for (int i = 0; i < length; i++) {
            int charWeight = input.charAt(i) - 'a' + 1;
            int currentWeight = charWeight;

            weights.add(currentWeight);

            for (int j = i + 1; j < length && input.charAt(j) == input.charAt(i); j++) {
                currentWeight += charWeight;
                weights.add(currentWeight);
                i = j;
            }
        }

        return weights;
    }
}
